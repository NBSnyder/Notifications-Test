/*Main Script*/

/*if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('sw.js').then(function() {
    console.log('Service Worker Registered');
  });
}*/
/*
function notify(x) {
  setTimeout(function(){
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
      var notification = new Notification("It works!");
    }
    else if (Notification.permission === "denied" || Notification.permission === "default") {
      Notification.requestPermission(function(){}).then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification("It works!");
          notification.onerror = alert("Error!!");
        }
        else {
          alert("Permission was not given.");
        }
      });
    }
    else {
      alert("Well, shit, something must've gone wrong.");
    }
  }, (x*1000));
}
*/

//Opera-default; Chrome-granted; Safari-denied; Firefox-granted
/*
function notify(x) {
  if (!(window.Notification)) {  // should work in Safari, Chrome, Firefox, and Opera.
    alert("Browser doesn't support notifications.");
  }
  else {
    alert(Notification.permission);
    alert(checkPermission());
    if (Notification.permission === "granted") {
      alert("will send notification.");
      var notification = new Notification("It works!");
      notification.onerror = alert("Error!!");
    }
    if (Notification.permission === "denied") {
      alert("Cannot send notifications.");
    }
    if (Notification.permission === "default") {
      alert("Will ask for permission to send notifications.");

    }
  }
}*/

var notify = function (title, options) {
    // Check for notification compatibility.
    if (!'Notification' in window) {
        // If the browser version is unsupported, remain silent.
        return;
    }

    console.log(Notification.permission);
    if (Notification.permission === 'default') {
        Notification.requestPermission(function () {
            notify(title, options);
        });
    }
    else if (Notification.permission === 'granted') {
        var n = new Notification(
                    title,
                    {
                      'body': title,
                      'tag' : title
                    }
                );
        n.onclick = function () {
            this.close();
        };
        n.onclose = function () {
            console.log('Notification closed');
        };
        n.onerror = function () {
            console.log('Error!!');
        };
    }
    // If the user does not want notifications to come from this domain...
    else if (Notification.permission === 'denied') {
        // ...remain silent.
        return;
    }
    return;
};
